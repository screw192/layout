import React from 'react';

import NavItems from "../NavItems/NavItems";
import "./MainNav.css";

const MainNav = () => {
  return (
      <header className="MainNavBlock">
        <div className="container">
          <div className="MainNav">
            <div className="MainNavLogo">
              Logo-shmlogo
            </div>
            <nav>
              <NavItems />
            </nav>
          </div>
        </div>
      </header>
  );
};

export default MainNav;