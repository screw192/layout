import React from 'react';

import NavItem from "./NavItem/NavItem";
import "./NavItems.css";

const NavItems = () => {
  return (
      <ul className="NavItems">
        <NavItem to={"/"} exact title={"Home"} />
        <NavItem to={"/schedule"} exact title={"Schedule"} />
        <NavItem to={"/faq"} exact title={"F.A.Q."} />
        <NavItem to={"/policy"} exact title={"Terms & Policy"} />
        <NavItem to={"/about"} exact title={"About Us"} />
        <NavItem to={"/contacts"} exact title={"Contact Us"} />
      </ul>
  );
};

export default NavItems;