import React from 'react';
import {NavLink} from "react-router-dom";

import "./NavItem.css";

const NavItem = ({to, exact, title}) => {
  return (
      <li className="NavItem">
        <NavLink to={to} exact={exact}>{title}</NavLink>
      </li>
  );
};

export default NavItem;