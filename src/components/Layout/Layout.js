import React from 'react';

import MainNav from "../Navigation/MainNav/MainNav";

const Layout = props => {
  return (
      <>
        <MainNav />
        <main className="Layout-Content">
          {props.children}
        </main>
      </>
  );
};

export default Layout;