import React from "react";
import {Route, Switch} from "react-router-dom";

import Layout from "./components/Layout/Layout";
import ContentPage from "./containers/ContentPage/ContentPage";
import AdminPage from "./containers/AdminPage/AdminPage";
import './App.css';

const App = () => (
    <Layout>
      <Switch>
        <Route path="/" exact component={ContentPage}/>
        <Route path="/admin" exact component={AdminPage}/>
        <Route path="/:id" exact component={ContentPage}/>
      </Switch>
    </Layout>
);

export default App;
