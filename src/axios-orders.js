import axios from "axios";

const axiosOrders = axios.create({
  baseURL: "https://js9-layout-screw192-default-rtdb.firebaseio.com/"
});

export default axiosOrders;