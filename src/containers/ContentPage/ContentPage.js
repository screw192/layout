import React, {useState, useEffect} from 'react';
import axiosOrders from "../../axios-orders";
import ReactHtmlParser from 'react-html-parser';

import "./ContentPage.css";

const ContentPage = props => {
  const [pageContent, setPageContent] = useState({});

  useEffect(() => {
    const fetchData = async () => {
      let fetchUrl;
      if (props.match.params.id) {
        fetchUrl = props.match.params.id + ".json";
      } else {
        fetchUrl = "home.json";
      }

      const response = await axiosOrders.get(fetchUrl);
      setPageContent(response.data);
    };

    fetchData().finally();
  }, [props.match.params.id]);

  return (
      <div className="ContentPage">
        <div className="container">
          <h4 className="Title">{pageContent.title}</h4>
          <p className="Content">{ReactHtmlParser(pageContent.content)}</p>
        </div>
      </div>
  );
};

export default ContentPage;