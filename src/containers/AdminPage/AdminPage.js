import React, {useState, useEffect} from 'react';
import axiosOrders from "../../axios-orders";
import { DefaultEditor } from 'react-simple-wysiwyg';

import "./AdminPage.css";

const AdminPage = props => {
  const [pages, setPages] = useState([]);
  const [targetPage, setTargetPage] = useState("");
  const [pageContent, setPageContent] = useState({
    title: "",
    content: ""
  });

  useEffect(() => {
    const fetchData = async () => {
      const response = await axiosOrders.get(".json");

      const pagesData = Object.keys(response.data);
      setPages(pagesData);
    };

    fetchData().finally();
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      if (targetPage !== "") {
        const response = await axiosOrders.get(targetPage + ".json");

        setPageContent(response.data);
      }
    };

    fetchData().finally();
  }, [targetPage]);

  const pagesOptions = pages.map(page => {
    return (
        <option
            key={page}
            value={page}
        >{page}</option>
    )
  });

  const targetSelect = event => {
    setTargetPage(event.target.value);
  };

  const inputHandler = event => {
    const title = event.target.value;

    setPageContent(prevState => ({
      ...prevState,
      title: title
    }));
  };

  const wysiwygHandler = event => {
    const text = event.target.value;

    setPageContent(prevState => ({
      ...prevState,
      content: text
    }));
  };

  const submitHandler = async event => {
    event.preventDefault();

    try {
      await axiosOrders.put(targetPage + ".json", pageContent);
    } finally {
      props.history.push("/" + targetPage);
    }
  };

  return (
      <div className="AdminPage">
        <div className="container">
          <form onSubmit={submitHandler}>
            <div className="FormRow">
              <label htmlFor="pages">Select page to edit:</label>
              <select
                  id="pages"
                  value={targetPage}
                  onChange={targetSelect}
              >
                <option value="" disabled hidden>...</option>
                {pagesOptions}
              </select>
            </div>
            <div className="FormRow">
              <label htmlFor="pageTitle">Title:</label>
              <input
                  type="text"
                  name="title"
                  id="pageTitle"
                  value={pageContent.title}
                  onChange={inputHandler}
              />
            </div>
            <div className="FormRow">
              <label htmlFor="pageContent">Title:</label>
              <DefaultEditor
                  value={pageContent.content}
                  onChange={wysiwygHandler}
              />
            </div>
            <button className="SaveButton">Save</button>
          </form>
        </div>

      </div>
  );
};

export default AdminPage;